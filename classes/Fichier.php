<?php

class Fichier {
    
    private $_name;
    private $_type;
    private $_tmp_name;
    private $_error;
    private $_size;

    public function __construct(array $fichier) {
		$this->hydrate($fichier);
	}

	public function hydrate(array $fichier)
	{
		foreach ($fichier as $cle => $valeur)
		{
			$methode = 'set'.ucfirst($cle);
	    
	    	if (method_exists($this, $methode)) {
	      		$this->$methode($valeur);
	    	}
        }
    }
    
    public function setName($name) {
        if(!empty($name)) {
            $this->_name = htmlentities($name);
        }
    }

    public function getName() { return $this->_name; }

    public function setType($type) {
        if(!empty($type)) {
            $this->_type = htmlentities($type);
        }        
    }

    public function getType() { return $this->_type; }

    public function setTmp_name($tmp_name) {
        if(!empty($tmp_name)) {
            $this->_tmp_name = htmlentities($tmp_name);
        }           
    }

    public function getTmp_name() { return $this->_tmp_name; }

    public function setError($error) {
        $error = (int) $error;
        $this->_error = $error;
    }

    public function getError() { return $this->_error; }

    public function setSize($size) {
        if(is_numeric($size)) {
            $this->_size = $size;
        }
    }

    public function getSize() { return $this->_size; }

    public function getTailleMax() { return Config::TAILLE_MAX; }

    /* On récupére le code d'erreur de file_uploads
    * On retourne le message d'erreur correspondant */
    private function MessageErreur($code) {
        $code = (int) $code;
        switch($code) {
            case 1:
                return "La taille du fichier téléchargé excède la valeur autorisée par le serveur !";
                break;
            case 2:
                return "La taille du fichier téléchargé excède la valeur autorisée !";
                break;
            case 3:
                return "Le fichier n'a été que partiellement téléchargé !";
                break;
            case 4:
                return "Aucun fichier n'a été téléchargé !";
                break;
            case 5:
                return "Fichier non pris en charge !";
                break;
            case 6:
                return "Un dossier temporaire est manquant !";
                break;
            case 7:
                return "Échec de l'écriture du fichier sur le disque !";
                break;
            case 8:
                return "Une extension PHP a arrêté l'envoi de fichier !";
                break;
            default:
                break;
        }
    }

    // On récupére le type Mime du fichier envoyé
    public function Mime() { 
        $info = getimagesize($this->_tmp_name);
        if($info == false) {
            return false;
        } else {
            list($appli,$type) = explode("/",$info['mime']);
            return $type;
        }
    }
}