<?php

class Images {
    
    private $_image;
    private $_largeur;
    private $_hauteur;

    public function __construct($image) {
        if(!empty($image)) {
            $this->_image = $image;
            list($this->_largeur, $this->_hauteur, $type, $attribut) = getimagesize($this->_image);
        }
    }

    // On redimensionne l'image si sa résolution dépasse le maximum autorisé
    public function Redimensionne() {

        // Est ce que les dimensions de l'image dépasse le maximum prévu ?
        if($this->_largeur > Config::LARGEUR_MAX OR $this->_hauteur > Config::HAUTEUR_MAX) {

            if(!isset($this->_image)) { return false; }

            if(!file_exists($this->_image)) { return false; }

            // On a besoin de la nouvelle hauteur ou de la nouvelle largeur
            if($this->_largeur > $this->_hauteur) {
                $reduction = ((Config::LARGEUR_MAX) * 100 / $this->_largeur);
                $nv_hauteur = intval(($this->_hauteur * $reduction) / 100);
            } elseif($this->_largeur < $this->_hauteur) {
                $reduction = ((Config::HAUTEUR_MAX) * 100 / $this->_hauteur);
                $nv_largeur = intval(($this->_largeur * $reduction) / 100);
            }

            if(!isset($nv_largeur)) { $nv_largeur = Config::LARGEUR_MAX; }
            if(!isset($nv_hauteur)) { $nv_hauteur = Config::HAUTEUR_MAX; }

            // Quel est l'extension du fichier ?
            $extension = substr(strtolower($this->_image),strripos($this->_image,"."));

            switch($extension) {
                case ".png":
                    $image_vierge = imagecreatefrompng($this->_image);
                    break;
                case ".jpg":
                case ".jpeg":
                    $image_vierge = imagecreatefromjpeg($this->_image);
                    break;
                case ".gif":
                    $image_vierge = imagecreatefromgif($this->_image);
                    break;
                default:
                    return false;
            }

            $nv_image = imagecreatetruecolor($nv_largeur,$nv_hauteur) or die("Erreur imagecreatetruecolor");
            
            imagecopyresampled($nv_image,$image_vierge,0,0,0,0,$nv_largeur,$nv_hauteur,$this->_largeur,$this->_hauteur);

            switch($extension) {
                case ".png":
                    imagepng($nv_image,$this->_image,0);
                    break;
                case ".jpg":
                case ".jpeg":
                    imagejpeg($nv_image,$this->_image,100);
                    break;
                case ".gif":
                    imagegif($nv_image,$this->_image);
                    break;
                default:
                    return false;
            }
        } 
    }   
}