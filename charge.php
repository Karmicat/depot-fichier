<?php

// Initialisation des messages
$retour = "";
$erreur = "";

$dossier = Config::IMG_DOSSIER;

// On mets le contenu du table $_FILES dans la classe Fichier
$fichier = new Fichier($_FILES['envoi']);

// A t-on une erreur avec file_uploads ?
if($fichier->getError() != 0) { 
    // Si oui, on récupére le message d'erreur
    $erreur = $fichier->MessageErreur($fichier->getError()); 
} else {
    // Liste des extensions autorisées
    $Extensions_Autorisees = array('jpg','jpeg','gif','png');
    // Si le Mime du fichier n'est pas dans la liste des extensions autorisées
    if(!in_array($fichier->Mime(),$Extensions_Autorisees)) {
        $erreur = "Seul les images sont autorisées !";
    } else {
        // Est ce que la taille du fichier dépasse la taille maximum autorisée ?
        if($fichier->getSize() > $fichier->getTailleMax()) {
            $erreur = "Taille maximum autorisée dépassée !";
        } else {
            // Est ce que le dossier de dépôt est présent ?
            if(!is_dir($dossier)) {
                // Si non, est ce que sa création s'est bien passé ?
                if(!mkdir($dossier,0777)) {
                    $erreur = "Impossible de créer le dossier /images/<br>Veuillez créer le dossier manuellement";
                }
            } else {
                // On génére le nouveau nom de fichier, avec le dossier au début
                $destination = $dossier . sha1($fichier->getName()) . "." . $fichier->Mime();
                // On envoie le fichier
                if(move_uploaded_file($fichier->getTmp_name(), $destination)) {
                    $image = new Images($destination);
                    $image->Redimensionne();
                    $retour = "Le fichier a bien été envoyé.";
                } else {
                    $erreur = "Le fichier n'a pas pu être envoyé !";
                }
            }
        }
    }
}

require_once join(DIRECTORY_SEPARATOR,['artsys','vues','defaut.phtml']);