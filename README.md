# Dépôt fichier

Dépôt fichier est une application de test. Elle n'est pas destinée à être mis en production en l'état.

## Dépendences

PHP 7 minimum

php-gd est utilisé pour le redimensionnement des images.

## Configuration

La configuration se fait via le fichier config.php

```php
switch(strtolower($_SERVER["HTTP_HOST"])) {
	case "localhost":
		class Config {
			// En dev ?
			const ENV_DEV = true;
			// Fuseau horaire
			const TIMEZONE = "Europe/Paris";
			// Dossier de dépot des images
			const IMG_DOSSIER = "./images/";
			// Largeur maximum des images (en pixel)
			const LARGEUR_MAX = 1024;
			// Hauteur maximum des images (en pixel)
			const HAUTEUR_MAX = 768;
			// Taille maximum des images (en octet)
			const TAILLE_MAX = 5000000000;
		}
		break;
	default:
		// Le domaine n'est pas le bon ? On arrête.
		die("ERREUR FATALE");
		break;
}
```

## Contributions

Toutes modifications sont les bienvenues, n'hésitez pas de prendre contact avec moi.

## Licence

[GNU GPL v3] (http://www.gnu.org/licenses/gpl-3.0.fr.html)