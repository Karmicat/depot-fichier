<?php

// Chargement automatique des classes

function chargerClasse($classe) {
    $LaClasse = join(DIRECTORY_SEPARATOR,['classes', ucfirst($classe) . '.php']);
    if(file_exists($LaClasse)) {
        require_once $LaClasse;
    }
}

spl_autoload_register('chargerClasse');
