<?php 

// Fichier de confiuration basé sur
// celui de https://github.com/querkmachine/placeponi.es

// On récupére le nom de domaine (en minuscule)
switch(strtolower($_SERVER["HTTP_HOST"])) {
	case "localhost":
		class Config {
			// En dev ?
			const ENV_DEV = true;
			// Fuseau horaire
			const TIMEZONE = "Europe/Paris";
			// Dossier de dépot des images
			const IMG_DOSSIER = "./images/";
			// Largeur maximum des images (en pixel)
			const LARGEUR_MAX = 1024;
			// Hauteur maximum des images (en pixel)
			const HAUTEUR_MAX = 768;
			// Taille maximum des images (en octet)
			const TAILLE_MAX = 5000000000;
		}
		break;
	default:
		// Le domaine n'est pas le bon ? On arrête.
		die("ERREUR FATALE");
		break;
}

// Définition du fuseau horaire
date_default_timezone_set(Config::TIMEZONE);

// Si en dev
if(Config::ENV_DEV) {
	// On affiche toutes les erreurs
	error_reporting(E_ALL);
}
else {
	// Pas en dev ? On n'affiche pas les erreurs.
	error_reporting(0);
}