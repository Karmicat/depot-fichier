<?php

/**
* Depot fichier
* =============
* Dépôt fichier est une application de test. 
* Elle n'est pas destinée à être mis en production en l'état.
*
* Karmicat - Juin 2020
* https://gitlab.com/Karmicat/depot-fichier
*
* Licence GNU GPLv3
**/

require_once 'autoload.php';
require_once 'config.php';

if(!isset($_GET['page'])) {
    require_once join(DIRECTORY_SEPARATOR,['artsys','vues','defaut.phtml']);
} else {
    $page = strtolower(htmlentities($_GET['page']));

    switch($page) {
        case "charge":
            require_once join(DIRECTORY_SEPARATOR,['charge.php']);
            break;
        default:
            require_once join(DIRECTORY_SEPARATOR,['artsys','vues','defaut.phtml']);
            break;
    }
}
